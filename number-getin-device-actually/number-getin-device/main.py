import paramiko
import re
import time
from dotenv import load_dotenv
from os import environ
from os.path import join, dirname, isdir
from extras.tools import (
    easy_rsa_dir,
    client_configs_dir,
    get_devices,
    build_config,
    scan_red
)
from extras.vpn import add_to_vpn
from extras.Models import Device
from mongoengine import connect
from pymongo.errors import ServerSelectionTimeoutError
from scp import SCPClient

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)


def main():

    if not isdir(easy_rsa_dir):
        print("Easy RSA is not installed. Expected: %s" % easy_rsa_dir)
        exit()

    if not isdir(client_configs_dir):
        print("Client configs dir not exists. Expected: %s" % client_configs_dir)
        exit()

    # get environ params
    device_user = environ.get("device_user")
    device_password = environ.get("device_password")
    model = environ.get("device_model")
    version = environ.get("device_version")
    device_prefix = environ.get("device_prefix")
    ca_host = environ.get("ca_host")
    ca_username = environ.get("ca_username")

    print(device_user, device_password, model, version, device_prefix)

    # Convert XXX.XXX.XXX.XXX to XXX.XXX.XXX.*
    network_ip = ".".join(environ.get("network_ip").split(".")[:-1])+".*"

    # Create conection to db
    connect(
        db=environ.get("db_name"),
        host=environ.get("db_host"),
        port=int(environ.get("db_port")),
        username=environ.get("db_username"),
        password=environ.get("db_password"),
        authentication_source=environ.get("db_authentication_source")
    )

    try:
        numbers = Device.get_available_numbers(device_prefix)
    except ServerSelectionTimeoutError:
        print("Error, tiempo limite a la conexion de la base de datos excedido.")
        exit()
    except Exception as e:
        print("Error inesperado", e)
        exit()

    # scan ips from red
    ips = scan_red(network_ip)

    # Get avaible devices ip
    devices = get_devices(ips, device_user, device_password)

    # Create ssh conection to flash device
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    # SSH conection to CA Server
    ca_ssh = paramiko.SSHClient()
    ca_ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    try:
        ca_ssh.connect(ca_host, username=ca_username)
    except paramiko.AuthenticationException as e:
        print("Could not connect to your CA machine")
        exit()

    ca_scp = SCPClient(ca_ssh.get_transport())

    # Namer counter
    namer = 0

    def next_number():
        if namer >= len(numbers):
            numbers.append(numbers[-1] + 1)
        return numbers[namer]

    # Array to write to /etc/hosts
    ok = []

    for device in devices:
        try:
            # SSH tunnel
            ssh.connect(device, username=device_user, password=device_password)

            # Client to scp
            scp = SCPClient(ssh.get_transport())

            # Get device's mac
            _, ssh_stdout, _ = ssh.exec_command("ifconfig")
            MAC = re.search(
                r'([0-9A-F]{2}[:-]){5}([0-9A-F]{2})',
                ssh_stdout.read(),
                re.I
            ).group()

            while True:

                # Create new hostname
                hostname = device_prefix+(str(next_number()).rjust(4, "0"))
                namer += 1

                print("hostname prospect: %s" % hostname)

                # Add hostname to ips database
                _, ssh_stdout, _ = ca_ssh.exec_command("./add-hostname %s" % hostname)

                response = ssh_stdout.read().replace("\n", "")

                if not response:
                    print("An ip has already been assigned to the hostname: %s.\nCheck this hostname because is not registered in mongo database."%hostname)
                    continue

                ips = response.split(",")

                try:
                    build_config(hostname, ca_ssh, ca_scp)
                except Exception as e:
                    print(e)
                    print("%s is available in both databases, but there is problem creating the config file.\nIt may be some problem in the CA server for this hostname."%hostname)
                    exit()

                break

            antena = Device(hostname, MAC, ips, model, version)

            try:
                antena.save()
            except Exception as e:
                print("La antena %s no se pudo guardar" % hostname)
                continue

            print(MAC, hostname, ips)

            # Append antena to ok lists
            ok.append(antena.device)

            # Send .conf file to antenna
            scp.put("%s/files/%s.conf" % (client_configs_dir, hostname), "/usr/local/allshoppings-cportal/openvpn/%s.conf"%hostname)

            # flash device
            command = "/usr/local/allshoppings-cportal/install.sh "+hostname
            ssh.exec_command(command)

            print("%s succesfully installed" % hostname)

            time.sleep(3)

            # restart device
            command = "reboot -f"
            ssh.exec_command(command, timeout=2)

            print("%s succesfully rebooted" % hostname)

        except Exception as e:
            # Don't care...
            print(e)
        finally:
            ssh.close()

    ca_ssh.close()

    print("Sendings ips to vpn...")

    add_to_vpn(ok)

    print("Added to vpn successfully")

    print("-----------------------------------------")
    for device in ok:
        print("| %s \t| %s \t|" % (device.mac, device.hostname))
    print("-----------------------------------------")

    return


if __name__ == '__main__':
    main()
