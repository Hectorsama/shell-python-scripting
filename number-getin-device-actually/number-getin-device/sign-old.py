import paramiko
from os import environ, system
from os.path import join, dirname, isdir
from dotenv import load_dotenv
from extras.tools import (
    easy_rsa_dir,
    client_configs_dir,
    build_config,
)
from extras.vpn import add_to_vpn
from mongoengine import connect
from pymongo.errors import ServerSelectionTimeoutError
from extras.Models import APDevice
from scp import SCPClient

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)

if not isdir(easy_rsa_dir):
    print("Easy RSA is not installed. Expected: %s" % easy_rsa_dir)
    exit()

if not isdir(client_configs_dir):
    print("Client configs dir not exists. Expected: %s" % client_configs_dir)
    exit()

# Create conection to db
connect(
    db=environ.get("db_name"),
    host=environ.get("db_host"),
    port=int(environ.get("db_port")),
    username=environ.get("db_username"),
    password=environ.get("db_password"),
    authentication_source=environ.get("db_authentication_source")
)

# SSH conection to CA Server
ca_ssh = paramiko.SSHClient()
ca_ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

try:
    ca_ssh.connect(environ.get("ca_host"), username=environ.get("ca_username"))
except paramiko.AuthenticationException as e:
    print("Could not connect to your CA machine")
    exit()

ca_scp = SCPClient(ca_ssh.get_transport())

ok = []

try:
    for apdevice in APDevice.objects.order_by("hostname"):

        print("Generating ip for %s" % apdevice.hostname)

        # Add hostname to ips database
        _, ssh_stdout, _ = ca_ssh.exec_command("./add-hostname %s" % apdevice.hostname)

        response = ssh_stdout.read().replace("\n", "")

        print(response)

        if not response:
            exit()

        apdevice.ips = response.split(",")

        try:
            build_config(apdevice.hostname, ca_ssh, ca_scp)
        except Exception as e:
            print(e)
            print("%s is available in both databases, but there is problem creating the config file.\nIt may be some problem in the CA server for this hostname."%hostname)
            exit()

        system("mv %s/files/%s.conf configs" % (client_configs_dir, apdevice.hostname))

        apdevice.save()

        ok.append(apdevice)

except ServerSelectionTimeoutError:
    print("Error, tiempo limite a la conexion de la base de datos excedido.")
    exit()

print("Sendings ips to vpn...")

add_to_vpn(ok)

print("Added to vpn successfully")