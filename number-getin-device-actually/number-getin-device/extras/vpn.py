import paramiko
import re
import time
from os import environ


def add_to_vpn(devices):
    ovpn_ssh = paramiko.SSHClient()
    ovpn_ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    ovpn_host = environ.get("ovpn_host")
    ovpn_username = environ.get("ovpn_username")
    ovpn_netaddress = environ.get("ovpn_netaddress")

    progress = 0

    # When a return, break or continue statement is 
    # executed in the try suite of a try...finally statement, 
    # the finally clause is also executed 'on the way out'.
    while True:
        try:
            ovpn_ssh.connect(ovpn_host, username=ovpn_username, timeout=10)
            print("Connection set succesfully")

            for device in devices[progress:]:

                device.ips[0] = xorips(ovpn_netaddress, device.ips[0])
                device.ips[1] = xorips(ovpn_netaddress, device.ips[1])

                etc_hosts_line = re.escape("%s %s" % (device.ips[0], device.hostname))

                ovpn_ssh.exec_command("echo 'ifconfig-push %s %s' > /tmp/%s" % (device.ips[0], device.ips[1], device.hostname))
                ovpn_ssh.exec_command("sudo mv /tmp/%s /etc/openvpn/ccd" % device.hostname)
                ovpn_ssh.exec_command("sudo sed '$a%s' -i /etc/hosts" % etc_hosts_line)

                # Print success
                print("%s added to VPN." % device.hostname)

                # Increase the progress
                progress += 1

                # Pause to exec next command
                time.sleep(1)

            # Restart vpn service
            ovpn_ssh.exec_command("sudo service openvpn@server restart")

            break
        except Exception as e:
            # Print the excpetion
            print(e)
            # Retry connect
            time.sleep(2)
        finally:
            # Close the connection
            ovpn_ssh.close()


def xorips(ip1, ip2):
    ip1 = ip1.split(".")
    ip2 = ip2.split(".")

    if len(ip1) != len(ip2):
        raise Exception("Arguments should be valid ip addresses.")

    final = [int(ip1[i]) ^ int(ip2[i]) for i, _ in enumerate(ip1)]

    return ".".join(map(str, final))