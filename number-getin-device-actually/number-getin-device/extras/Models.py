import datetime
from tools import missing_numbers
from mongoengine import (
    Document,
    StringField,
    IntField,
    FloatField,
    LongField,
    BooleanField,
    DateTimeField,
    ListField,
    DictField
)


class APDevice(Document):
    key = StringField(primary_key=True)

    hostname = StringField(max_length=255, required=True)
    description = StringField(max_length=255)

    model = StringField(max_length=255)
    version = StringField(max_length=255)
    mac = StringField(max_length=255)

    external = BooleanField(default=False)

    lon = FloatField(default=0.0)
    lat = FloatField(default=0.0)

    lastUpdate = DateTimeField(default=datetime.datetime.now)
    creationDateTime = DateTimeField(default=datetime.datetime.now)

    ips = ListField(default=[])

    mode = StringField(default="master")

    meta = {'collection': 'APDevice'}

    def setId(self, hostname):
        self.hostname = hostname
        self.id = 'APDevice("%s")' % hostname


class APDReport(Document):
    key = StringField(primary_key=True)

    hostname = StringField(max_length=255, required=True)
    reportable = BooleanField(default=False)

    reportStatus = IntField(default=0)
    lastRecordCount = IntField(default=0)
    status = IntField(default=0)

    excelReportDate = DictField()
    excelPeriod = IntField(default=0)

    lastUpdate = DateTimeField(default=datetime.datetime.now)
    creationDateTime = DateTimeField(default=datetime.datetime.now)
    lastRecordDate = DateTimeField(default=datetime.datetime.now)

    meta = {'collection': 'APDReport'}

    def setId(self, hostname):
        self.hostname = hostname
        self.id = 'APDReport("%s")' % hostname


class APDCalibration(Document):
    key = StringField(primary_key=True)

    visitsOnWed = BooleanField(default=True)
    visitsOnTue = BooleanField(default=True)
    visitsOnThu = BooleanField(default=True)
    visitsOnSun = BooleanField(default=True)
    visitsOnSat = BooleanField(default=True)
    visitsOnMon = BooleanField(default=True)
    visitsOnFri = BooleanField(default=True)

    visitStartWed = StringField(max_length=5, default="11:00")
    visitStartTue = StringField(max_length=5, default="11:00")
    visitStartThu = StringField(max_length=5, default="11:00")
    visitStartSun = StringField(max_length=5, default="11:00")
    visitStartSat = StringField(max_length=5, default="11:00")
    visitStartMon = StringField(max_length=5, default="11:00")
    visitStartFri = StringField(max_length=5, default="11:00")

    visitPowerThreshold = IntField(default=0)
    visitMinTimeThreshold = IntField(default=0)
    visitMinTimeSlotPercentage = FloatField(default=0.0)
    visitMaxTimeThreshold = IntField(default=21600000)
    visitGapThreshold = IntField(default=10)

    visitEndWed = StringField(max_length=5, default="20:00")
    visitEndTue = StringField(max_length=5, default="20:00")
    visitEndThu = StringField(max_length=5, default="20:00")
    visitEndSun = StringField(max_length=5, default="20:00")
    visitEndSat = StringField(max_length=5, default="20:00")
    visitEndMon = StringField(max_length=5, default="20:00")
    visitEndFri = StringField(max_length=5, default="20:00")

    visitCountThreshold = LongField(default=0)
    viewerPowerThreshold = IntField(default=0)
    viewerMinTimeThreshold = IntField(default=60000)
    viewerMaxTimeThreshold = IntField(default=180000)

    timeDecay = IntField(default=0)
    repeatThreshold = IntField(default=5)
    powerDecay = IntField(default=0)
    peasantPowerThreshold = IntField(default=0)
    offSetViewer = IntField(default=0)

    monitorStart = StringField(max_length=5, default="09:00")
    monitorEnd = StringField(max_length=5, default="21:00")

    lastUpdate = DateTimeField(default=datetime.datetime.now)
    hostname = StringField(max_length=255, required=True)

    generateViewers = BooleanField(default=False)
    generatePedestrians = BooleanField(default=False)
    creationDateTime = DateTimeField(default=datetime.datetime.now)

    meta = {'collection': 'APDCalibration'}

    def setId(self, hostname):
        self.hostname = hostname
        self.id = 'APDCalibration("%s")' % hostname


class Device():
    def __init__(self, hostname, mac, ips, model, version):
        self.hostname = hostname

        self.device = APDevice()
        self.device.mac = mac
        self.device.model = model
        self.device.version = version
        self.device.ips = ips
        self.device.setId(hostname)

        self.report = APDReport()
        self.report.setId(hostname)

        self.calibration = APDCalibration()
        self.calibration.setId(hostname)

    @staticmethod
    def get_available_numbers(prefix):
        devices = APDevice.objects(hostname__contains=prefix).order_by("hostname")

        if not devices:
            return 1

        numbers_list = []

        for device in devices:
            try:
                numbers_list.append(int(device.hostname.split(prefix)[1]))
            except Exception as _:
                continue

        return missing_numbers(numbers_list)

    def save(self):
        self.device.save()
        self.report.save()
        self.calibration.save()
