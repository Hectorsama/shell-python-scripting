import paramiko, subprocess, re
from os.path import expanduser
from os import system
from shutil import copyfile

home_dir = expanduser("~")
client_configs_dir = "%s/client-configs" % home_dir
easy_rsa_dir = "%s/EasyRSA-v3.0.6" % home_dir


def scan_red(ip_red):
    # Escaneo usando nmap
    print("Buscando antenas")
    scan = subprocess.check_output(['nmap', '-sP', ip_red])
    return re.findall(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', scan)


def get_devices(ips, device_user, device_password):
    antenas = []

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    for ip in ips:
        try:
            # Intento de conexion para verificar cuales son antenas candidatas
            ssh.connect(ip, username=device_user, password=device_password, timeout=10)
            _, ssh_stdout, _ = ssh.exec_command("cat /proc/sys/kernel/hostname")
            user = ssh_stdout.read().replace("\n", "")

            if user == "getin":
                print("%s: antena valida!" % ip)
                antenas.append(ip)
            else:
                _, ssh_stdout, _ = ssh.exec_command("ifconfig")
                MAC = re.search(r'([0-9A-F]{2}[:-]){5}([0-9A-F]{2})', ssh_stdout.read(), re.I).group()
                print("["+user+",IP:"+ip+",MAC:"+MAC+"]")
            ssh.close()
        except Exception as e:
            print(e)
    # Show finded antena
    print(antenas, len(antenas))
    return antenas


def build_config(hostname, ssh, scp):
    # Generate the .req
    command = "{0}/easyrsa --pki-dir={0}/pki gen-req {1} nopass batch".format(easy_rsa_dir, hostname)
    if system(command) != 0:
        raise Exception("There was an error creating the %s.req" % hostname)

    # Copy file to client configs
    copyfile("%s/pki/private/%s.key" % (easy_rsa_dir, hostname), "%s/keys/%s.key" % (client_configs_dir, hostname))

    print("Sending %s.req to CA machine" % hostname)

    # Send .req to windu
    scp.put("%s/pki/reqs/%s.req" % (easy_rsa_dir, hostname), "/tmp")

    # Sign the certificate
    _, std_out, _ = ssh.exec_command("./remote-sign %s >> /dev/null 2>&1; echo $?" % hostname)

    response = std_out.read().replace("\n", "")

    # Check for response.
    if int(response) != 0:
        raise Exception("There was an error signing the %s.req" % hostname)

    # Copy the .crt
    scp.get("/tmp/%s.crt" % hostname, "%s/keys/%s.crt" % (client_configs_dir, hostname))

    print("%s.crt copied successfully." % hostname)

    # Generate the config
    if system("%s/make_config.sh %s" % (client_configs_dir, hostname)) != 0:
        raise Exception("There was an error creating the config file %s.conf" % hostname)


def missing_numbers(num_list):
    original_list = [x for x in range(1, num_list[-1] + 1)]
    return list(set(num_list) ^ set(original_list))


def dec_to_ip(ipint):
    # Max can be 65535
    ip = ""
    for i in range(2):
        ip1 = ""
        for j in range(8):
            ip1 = str(ipint % 2)+ip1
            ipint = ipint >> 1
        ip = str(int(ip1, 2)) + "." + ip
    return ip.strip(".")
