# devices-end-point-checker

## Install
1. Install pip2 `sudo apt install python-pip`
2. Install virtualenv `sudo pip install virtualenv`
3. Install nmap `sudo apt install nmap` or download [here](https://nmap.org/download.html)
4. Clone project `git clone https://gitlab.com/getin_mx/number-getin-device.git`
6. Open project `cd number-getin-device`
7. Create Python enviroments  `virtualenv venv --python=python2`
8. Activate env `source venv/bin/activate`
9. Copy .env.example to .env `cp .env.example .env`

## Config .env
### Default config
>>>
db_host = ""
db_name = ""
db_port = ""
db_username = ""
db_password = ""
db_authentication_source="admin"

device_user = "root"
device_password = ""
device_model = "TL-MR3020"
device_version = "3.20"
device_prefix = "test-"

network_ip = "192.168.1.0"

ca_host = ""
ca_username = ""

ovpn_host=""
ovpn_username=""
ovpn_netaddress="10.4.0.0"
>>>
### Properties

| Property                  | Meaning                                                           |
| --------                  | --------                                                          |
| db_host                   | the host name of the mongod instance to connect to                |
| db_name                   | the name of the database to use, for compatibility with connect   |
| db_port                   | the port that the mongod instance is running on                   |
| db_username               | username to authenticate with                                     |
| db_password               | password to authenticate with                                     |
| db_authentication_source  | database to authenticate against                                  |
| device_user               | name of device root user                                          |
| device_password           | password to login as root user                                    |
| device_model              | Device model without version                                      |
| device_version            | Device version                                                    |
| device_prefix             | prefix of device name (gihs-,test-)                               |
| network_ip                | ip where program will search devices                              |

## Run it
1. Open project folder
2. Activate python env (`source venv/bin/activate`)
3. Run: `python main.py <init_number>`
