#!/bin/bash
if [ -z $1 ]; then
    echo "${red}Parameter required, parameter must be hostname"
    echo "Example :/get_mac_device_sigle gihs-0557 "
    exit
fi

ping=$(ping -q -c 1 -W 1 $1 >/dev/null 2>&1; echo $?)

if [[ $ping != 0 ]]; then
echo "DeviceNotFound"
elif mac_device=$(sshpass -p xcal#bury5443 ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  -o LogLevel=QUIET root@$1 "ifconfig -a | grep eth " | awk '{print $5}');  
then
 echo "$1 $mac_device"
fi
