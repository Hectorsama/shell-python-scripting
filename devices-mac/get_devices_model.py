#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pymongo import MongoClient, ASCENDING
from dotenv import load_dotenv
from os.path import join, dirname, isdir
from os import environ
import errno
import time
import dateutil.parser
from socket import error as socket_error
import paramiko,subprocess,re
from paramiko.ssh_exception import BadHostKeyException, AuthenticationException, SSHException, NoValidConnectionsError

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)

dbname=str(environ.get("db_name"))
host=environ.get("db_host")
port=environ.get("db_port")
username=environ.get("db_username")
password=environ.get("db_password")
collection=environ.get("db_collection")
connection="mongodb://"+username+":"+password+"@"+host+":"+port
client = MongoClient(connection)
db = client[dbname]
APDeviceColum = db[collection]
APDReport=db["APDReport"]


ovpn_ssh = paramiko.SSHClient()
ovpn_ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ovpn_host = environ.get("ovpn_host")
ovpn_username = environ.get("ovpn_username")
ovpn_netaddress = environ.get("ovpn_netaddress")

try: db.command("serverStatus")
except Exception as e: print(e)
else: print("You are connected!")
APDeviceColum = db[collection]
APDReport=db["APDReport"]

dateTime=dateutil.parser.parse("2020-01-01T00:00:00.000Z") 
device_report=({"lastRecordDate" : {"$gte": dateTime}})
query_report =APDReport.find(device_report).sort([("hostname", ASCENDING)])
antenas=[x["hostname"] for x in query_report]

query={'$or': [{'model': {'$exists': False}}, {'model': ''}]}
#Give devices without Model
for hostname in APDeviceColum.find(query).sort([("hostname", ASCENDING)]):
    try:
        if str(hostname['hostname']) not in antenas:
            continue

        ovpn_ssh.connect(ovpn_host, username=ovpn_username,timeout=20)
        cmd="/root/get_model_version_device_single.sh {}".format(str(hostname['hostname']))
        ssh_stdin, ssh_stdout, ssh_stderr= ovpn_ssh.exec_command(cmd)
        device = ssh_stdout.read()
        if len(device.split()) < 2:
			print("Connection refused"+ " " + str(hostname['hostname']))
			continue
       
        model=device.split()[1]
        myquery = { "hostname":str(hostname['hostname'])}
        newvalues ={ "$set": { "model": model} }
        APDeviceColum.update_one(myquery,newvalues)
        print("Connection succesfully to "+ str(hostname['hostname']))   
    except (BadHostKeyException, AuthenticationException, SSHException, NoValidConnectionsError) as e:
		print(e)
    except Exception as we:
        print(we)
    except  socket_error as ex:
        print(ex)
    else:
        ovpn_ssh.close()
        client.close()
