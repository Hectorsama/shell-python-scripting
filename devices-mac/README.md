## Install
1. Install pip2 `sudo apt install python-pip`
2. Install virtualenv `sudo pip install virtualenv`
3. Clone project `git clone https://gitlab.com/getin_mx/get-devices-mac.git`
4. Create Python enviroments  `virtualenv venv --python=python2`
5. Activate env `source venv/bin/activate`
6. Copy .env.example to .env `cp .env.example .env`
7. Set default values .env
8. Install dependencies `pip install -r requeriments.txt.`
## Config .env
### Default config
>>>

db_host = 
db_name =
db_port = 
db_username =
db_password = 
db_authentication_source=
db_collection=

ovpn_host=
ovpn_username=
ovpn_netaddress=

>>>
### Properties

| Property                  | Meaning                                                           |
| --------                  | --------                                                          |
| db_host                   | the host name of the mongod instance to connect to                |
| db_name                   | the name of the database to use, for compatibility with connect   |
| db_port                   | the port that the mongod instance is running on                   |
| db_username               | username to authenticate with                                     |
| db_password               | password to authenticate with                                     |
| db_authentication_source  | database to authenticate against                                  |
| db_collection             | name collection database                                          |
| ovpn_host                 | hostname vpn                                                      |
| ovpn_username             | username vpn, should be a root                                    |
| ovpn_netaddress           | ip vpn                                                            |

## Run it
1. Open project folder  `cd get-devices-mac`
2. Activate python env (`source venv/bin/activate`)
3. Run: `python get_devices_mac.py` or `python get_devices_model.py`
