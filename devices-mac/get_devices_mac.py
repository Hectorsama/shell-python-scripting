from pymongo import MongoClient, ASCENDING
from dotenv import load_dotenv
from mongoengine import connect
from os.path import join, dirname, isdir
from os import environ
import errno
import dateutil.parser
from socket import error as socket_error
import paramiko,subprocess,re
from paramiko.ssh_exception import BadHostKeyException, AuthenticationException, SSHException, NoValidConnectionsError

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)

dbname=str(environ.get("db_name"))
host=environ.get("db_host")
port=environ.get("db_port")
username=environ.get("db_username")
password=environ.get("db_password")
collection=environ.get("db_collection")
connection="mongodb://"+username+":"+password+"@"+host+":"+port

ovpn_ssh = paramiko.SSHClient()
ovpn_ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ovpn_host = environ.get("ovpn_host")
ovpn_username = environ.get("ovpn_username")
ovpn_netaddress = environ.get("ovpn_netaddress")
client = MongoClient(connection)
db = client[dbname]
APDeviceColum = db[collection]

try: db.command("serverStatus")
except Exception as e: print(e)
else: print("You are connected!")
collist = db.list_collection_names()
APDeviceColum = db[collection]
hostnames_db =''
APDReport=db["APDReport"]

dateTime=dateutil.parser.parse("2020-01-01T00:00:00.000Z") 
device_report=({"lastRecordDate" : {"$gte": dateTime}})
query_report =APDReport.find(device_report).sort([("hostname", ASCENDING)])
antenas=[str(x["hostname"]) for x in query_report]

query={'$or': [{'mac': {'$exists': False}}, {'mac': ''}]}
#Give devices without MAC reported 2020
for hostname in APDeviceColum.find(query).sort([("hostname", ASCENDING)]):
	hostname_value=str(hostname['hostname'])
	try:
		if hostname_value not in antenas:
			continue
		print("Connect to "+ hostname_value)
		f = open("test.txt", "a+")
		ovpn_ssh.connect(ovpn_host, username=ovpn_username, timeout=20)
		cmd = "/root/get_mac_device_single.sh {}".format(hostname_value)
		ssh_stdin, ssh_stdout, ssh_stderr = ovpn_ssh.exec_command(cmd)
		device=ssh_stdout.read()
		devices = [x for x in device.split()] 
		if len(device.split()) < 2:
			print('Without hostname or not connection')
			continue
		device_name=str(devices[0])
		device_mac=str(devices[1])
		print("Device: "+device_name+" MAC: "+ device_mac)
		myquery = { "hostname":device_name}
		newvalues = { "$set": { "mac": device_mac} }
		APDeviceColum.update_one(myquery,newvalues)
		print("Connection succesfully "+ hostname_value)
		f.write("%s\n" % device_name)
	except (BadHostKeyException, AuthenticationException, SSHException, NoValidConnectionsError) as e:
		print(e)
	except  socket_error as ex:
		print(ex)
	
	ovpn_ssh.close()
	client.close()


