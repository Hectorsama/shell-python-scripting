from pymongo import MongoClient, ASCENDING
from dotenv import load_dotenv
from mongoengine import connect
from os.path import join, dirname, isdir
from os import environ
import errno
from socket import error as socket_error
import paramiko,subprocess,re

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)

dbname=str(environ.get("db_name"))
host=environ.get("db_host")
port=environ.get("db_port")
username=environ.get("db_username")
password=environ.get("db_password")
collection=environ.get("db_collection")
connection="mongodb://"+username+":"+password+"@"+host+":"+port


ovpn_host = environ.get("ovpn_host")
ovpn_username = environ.get("ovpn_username")
ovpn_netaddress = environ.get("ovpn_netaddress")

ovpn_ssh = paramiko.SSHClient()
ovpn_ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ovpn_ssh.connect(ovpn_host, username=ovpn_username,timeout=20)

client = MongoClient(connection)
db = client[dbname]


try: db.command("serverStatus")
except Exception as e: print(e)
else: print("You are connected!")
APDevice = db["APDevice"]



#query={'$or' : [ {'model':"TL-MR3020"},{'model':"TL-MR3020v3"}],'hasVPNConfig': True}
query={'hostname':"gihs-0557"}
#Give devices
with open('success.txt', 'r+') as g:
    g_read = g.read().split('\n')
    for hostname in APDevice.find(query).sort([("hostname", ASCENDING)]): 
        hostname_value=str(hostname['hostname'])
        if hostname_value in g_read:
            continue
        cmd="/root/reboot_button.sh {}".format(hostname_value)
        try:
            #ssh_stdin, ssh_stdout, ssh_stderr = ovpn_ssh.exec_command(cmd)
            #response = ssh_stdout.read()
        except Exception as e:
            print(e)
            continue
        print(str(response, 'utf-8'))
        g.write("%s\n" % hostname_value)

client.close()
g.close()
ovpn_ssh.close()
