import datetime
from dotenv import load_dotenv
import getopt
from os import environ
from os.path import join, dirname
import sys
import time
from getin import (
    Assignation,
    Calibration,
    Uptime,
    InnerZone,
    EntityKind,
    DashboardIndicatorData,
    Element,
    Store
)
from getin.util import connect_db
from pytz import timezone
import pytz
import statistics


# load .env
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

mexicoCity = timezone('America/Mexico_City')

oneDay = datetime.timedelta(days=1)
sixhrs = datetime.timedelta(hours=6)

def createDID(entityKind, entityId, subentityName, subentityId, date, record):
    # Hotfix: Add 6 hours to date because getin-servers are in America/Mexico_City timezone and it affects to queries on database.
    # TODO: Remove the 6 hours when datanucleus on getin-server is fixed.
    date = date + sixhrs

    did = DashboardIndicatorData.updateOrCreate({
        "timeZone"      : 0,
        "subentityId"   : subentityId,
        "stringDate"    : date.strftime('%Y-%m-%d'),
        "entityKind"    : entityKind,
        "entityId"      : entityId,
        "elementSubName": Element.SUBNAME_UPTIME,
        "elementSubId"  : Element.SUBID_UPTIME,
        "elementName"   : Element.NAME_UPTIME,
        "elementId"     : Element.ID_UPTIME,
        "dayOfWeek"     : int(date.strftime('%w')) + 1,
        "date"          : date,
        "recordCount"   : 0
    }, {
        "subentityName" : subentityName,
        "doubleValue"   : record
    })
    print("Uptime for {} [{}] was created succesfully.".format(did.subentityId, str(did.date)))

def calculateUptime(calibration, date, storeTimezone, uptimes):
    if not getattr(calibration,  'visitsOn%s' % date.strftime('%a')):
        return float(0)

    openHour = '11:00' # defaults
    closeHour = '20:30' # defaults

    if hasattr(calibration, 'visitStart%s' % date.strftime('%a')):
        openHour = getattr(calibration,  'visitStart%s' % date.strftime('%a'))

    if hasattr(calibration, 'visitEnd%s' % date.strftime('%a')):
        closeHour = getattr(calibration,  'visitEnd%s' % date.strftime('%a'))

    oDate = date

    splitedOpenHour = openHour.split(':')
    splitedCloseHour = closeHour.split(':')

    datetimeOH = storeTimezone.localize(
        datetime.datetime(
            oDate.year,
            oDate.month,
            oDate.day,
            int(splitedOpenHour[0]),
            int(splitedOpenHour[1])
        )
    ).astimezone(mexicoCity)

    # If the close hour is less than open hour it close next day 
    if int(splitedCloseHour[0]) > int(splitedOpenHour[0]):
        cDate = date
    else:
        cDate = date + oneDay

    datetimeCH = storeTimezone.localize(
        datetime.datetime(
            cDate.year,
            cDate.month,
            cDate.day, 
            int(splitedCloseHour[0]),
            int(splitedCloseHour[1])
        )
    ).astimezone(mexicoCity)

    allUptime = []

    for uptime in uptimes:
        dateSplited = uptime.date.split('-')
        for interval in uptime.record:
            intervalSplited = interval.split(':')
            allUptime.append({
                'datetime': mexicoCity.localize(
                    datetime.datetime(
                        int(dateSplited[0]),
                        int(dateSplited[1]),
                        int(dateSplited[2]),
                        int(intervalSplited[0]),
                        int(intervalSplited[1]),
                    )
                ),
                'value': uptime.record[interval]
            })

    # Filter uptime between open and close hours
    importantUptime = [entry for entry in allUptime if entry['datetime'] > datetimeOH and entry['datetime'] < datetimeCH]

    # From filter find when antennas was on
    onUptime = [entry for entry in importantUptime if entry['value'] == 1]

    # Get the percentage
    uptime = 0 if len(importantUptime) - len(onUptime) >= 24 else len(onUptime) * 100 / max(1, len(importantUptime))

    return float(uptime)

def buildDateUptime(date, calibrations, stores, innerZones, entityIds):

    activeAssignations = Assignation.objects(__raw__ = {
        'fromDate': { '$lte' : date },
        '$or': [
            { 'toDate'  : { '$exists': False } },
            { 'toDate'  : { '$gt': date  } },
            { 'toDate'  : None},
        ],
        'entityId': { '$in': entityIds}
    })

    uptimes = Uptime.objects(__raw__ = {
        '$or': [
            { 'date': date.strftime('%Y-%m-%d') },
            { 'date': (date - oneDay).strftime('%Y-%m-%d') }
        ],
        'hostname': { '$in': [assignation.hostname for assignation in activeAssignations] }
    })

    def getParent(assignable):
        if assignable.entityKind == EntityKind.KIND_INNERZONE:
            return next((innerZone for innerZone in innerZones if innerZone.cleanId() == assignable.entityId), None)
        else: 
            return next((store for store in stores if store.cleanId() == assignable.entityId), None)

    zoneUptimes = {}

    # Inner zones uptime
    for assignation in (assignation for assignation in activeAssignations if assignation.entityKind == EntityKind.KIND_INNERZONE):
        # Get the calibration
        calibration = next((calibration for calibration in calibrations if calibration.hostname == assignation.hostname), None)
        
        # Get the inner zone
        innerZone = next((innerZone for innerZone in innerZones if innerZone.cleanId() == assignation.entityId), None)
        
        # Find until parent is a store
        child = innerZone
        while True:
            store = getParent(child)
            if child.entityKind == EntityKind.KIND_STORE:
                break
            else: 
                child = store

        print("Starting uptime for {}".format(assignation.hostname))

        # Calculate the uptime
        uptime = calculateUptime(
            calibration,
            date,
            timezone(store.timezone),
            (uptime for uptime in uptimes if uptime.hostname == assignation.hostname)
        )

        # Group zone uptimes by store
        if store.cleanId() in zoneUptimes:
            zoneUptimes[store.cleanId()].append(uptime)
        else:
            zoneUptimes[store.cleanId()] = [uptime]

        createDID(
            EntityKind.KIND_INNERZONE,
            innerZone.cleanId(),
            innerZone.name,
            innerZone.cleanId(),
            date,
            uptime
        )

    # Stores uptime excluding the ones with inner zones.
    for assignation in (assignation for assignation in activeAssignations if assignation.entityKind == EntityKind.KIND_STORE):
        # Search it in stores with zones
        if assignation.entityId in zoneUptimes:
            continue
        
        # Get the calibration
        calibration = next((calibration for calibration in calibrations if calibration.hostname == assignation.hostname), None)
        
        # Get the store
        store = next((store for store in stores if store.cleanId() == assignation.entityId), None)

        print("Starting uptime for {}".format(assignation.hostname))

        if store is None:
            print(assignation.to_json())
            continue

        # Calculate the uptime
        uptime = calculateUptime(
            calibration,
            date,
            timezone(store.timezone),
            (uptime for uptime in uptimes if uptime.hostname == assignation.hostname)
        )

        createDID(
            EntityKind.KIND_BRAND,
            store.brandId,
            store.name,
            store.cleanId(),
            date,
            uptime
        )

    # Calculate the uptime for stores with innerzones.
    for storeId in zoneUptimes:
        # Get the store
        store = next((store for store in stores if store.cleanId() == storeId), None)

        # Get the zones that are active
        active = [uptime for uptime in zoneUptimes[storeId] if uptime != 0]

        if (len(active) / max(1, len(zoneUptimes[storeId]))) * 100 > 60:
            uptime = statistics.mean(active)
        else:
            uptime = 0

        createDID(
            EntityKind.KIND_BRAND,
            store.brandId,
            store.name,
            store.cleanId(),
            date,
            uptime
        )

def getAllEntities(brandId, stores, innerZones):
    allParentsAreStores = False
    storesByBrand = [store for store in stores if store.brandId == brandId]
    entities = storesByBrand

    for store in storesByBrand:
        childs = getInnerZones(store, innerZones)
        entities += childs

    return entities

def getInnerZones(entity, innerZones):
    myzones = [innerZone for innerZone in innerZones if innerZone.entityId == entity.cleanId()]
    childs = myzones

    for innerZone in myzones:
        childs += getInnerZones(innerZone, innerZones)

    return childs

def main():

    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["fromDate=", "toDate=", "brand=", "entities="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))  # will print something like "option -a not recognized"
        sys.exit(2)
    
    # Initizalize defaults
    fromDate = datetime.datetime.combine(
        datetime.datetime.now() - oneDay,
        datetime.time.min
    )

    toDate = fromDate

    brandId = ""
    entityIds = []

    for o, a in opts:
        if o == "--fromDate":
            fromDate = datetime.datetime.strptime(a, '%Y-%m-%d')
        elif o == "--toDate":
            toDate = datetime.datetime.strptime(a, '%Y-%m-%d')
        elif o == "--brand":
            brandId = a
        elif o == "--entities":
            entityIds = a.split(',')
        else:
            assert False, "unhandled option"


    # get the default db connection
    connect_db()

    calibrations = Calibration.objects
    stores = Store.objects
    innerZones = InnerZone.objects

    if not brandId:
        entities = list(stores) + list(innerZones)
    else:
        entities = getAllEntities(brandId, list(stores), list(innerZones))

    if len(entityIds) == 0:
        entityIds = [entity.cleanId() for entity in entities]

    while fromDate <= toDate:
        buildDateUptime(fromDate, calibrations, stores, innerZones, entityIds)
        # Generate the uptime of the next day
        fromDate += oneDay

    print("Uptime was generated succesfully")
    
if __name__ == '__main__':
    main()
    