# Daily uptime

## Install
1. Install pip `sudo apt install python-pip`
2. Install virtualenv `sudo pip install virtualenv`
3. Clone project `git clone https://gitlab.com/getin_mx/daily-uptime.git`
4. Go to project `cd daily-uptime`
5. Create Python enviroments  `virtualenv venv --python=python3.6`
6. Copy .env.example to .env `cp .env.example .env`
7. Activate virtual env `source venv/bin/activate`
8. Install dependencies `pip install -r requirements.txt`

### Properties

| Property                  | Meaning                                                           |
| --------                  | --------                                                          |
| DB_NAME=                  | the host name of the mongod instance to connect to                |
| DB_HOST=                  | the name of the database to use, for compatibility with connect   |
| DB_PORT=                  | the port that the mongod instance is running on                   |
| DB_USERNAME=              | username to authenticate with                                     |
| DB_PASSWORD=              | password to authenticate with                                     |
| DB_AUTHENTICATION_SOURCE= | database to authenticate against                                  |

### Run

1. Go to project `cd daily-uptime`
2. Activate env `source venv/bin/activate`
3. Run: `python main.py [OPTIONS]` 

### Options

| Options     | Description                                  |
| --------    | --------                                     |
| --fromDate= | Date from which the uptime will be generated |
| --toDate=   | Date to which the uptime will be generated   |

### Issues

Update the last tag version from getin models to avoid problems with the models
